<div align="center">
  <img width="64" src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/21559004/iconfinder_fast-website-load-speed_4417092.png?width=64" alt="Dashboard Covid">
  <h3 align="center">Servidor proxy</h3>
  <p align="center">
    Reverse proxy para acceder al la API y a la aplicación web
  </p>
</div>

Resultado del proyecto

<div align="center">
  <img src="/img/dashboard.png" alt="Dashboard Covid">
</div>

Para ejecutar el proxy es necesario descargar el repositorio Dashboard Covid y Backend COVID, por simplicidad, puedes tener tus directorios de la siguiente forma:

* Proxy server
	* Dashboard Covid
	* Backend 
	
En este repositorio ya se encuentran excluidos los directorios para que no se suban al repositorio y se mantenga de forma aislada

## Arquitectura

Al levantar los ambientes se crea lo siguiente

<div align="center">
  <img src="/img/architecture.png" alt="Dashboard Covid">
</div>

## Ramas

Basado en GitFlow

* Staging para despliegues en ambiente de pruebas
* Master para enviar a producción
* Ramas creadas por issues con merge request, para añadir caracteristicas, mejoras y corregir errores

## Fácil gestión de ambientes y separación de responsabilidades

El proyecto está separado por ambientes y es facilmente adaptable con validaciones de CI/CD

<div align="center">
  <img src="/img/deploy.png" alt="Dashboard Covid">
</div>

## Preparado para kubernetes

Cada repositorio incluye sus respectivas configuraciones para poder funcionar en un cluster de Kubernetes, incluída la configuración del grupo

> Se encuentra en desarrollo el despliegue automatizado

# Ejecución con Docker
## Aplicación de desarrollo en local

```sh
docker-compose up
```

Al ejecutar se levantaran los contenedores con terminación dev
<div align="center">
  <img src="/img/development.png" alt="Dashboard Covid">
</div>

## Aplicación para producción
```sh
docker-compose -f docker-compose.prod.yml up
```

Contenedores con configuraciones productivas

<div align="center">
  <img src="/img/development.png" alt="Dashboard Covid">
</div>

Al finalizar la aplicación con los contenedores quedará arriba y podrás acceder desde:

http://localhost:5000

http://localhost:5000/api