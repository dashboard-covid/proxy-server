FROM nginx:1.19.2-alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY environments/development/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]